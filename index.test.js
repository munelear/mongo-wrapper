const MongoWrapper = require('./index');
const actualMongoWrapper = require('./src/mongoWrapper');

test('it should export MongoWrapper', () => {
  expect(MongoWrapper).toEqual(actualMongoWrapper);
});