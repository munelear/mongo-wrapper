const MongoClient = require('mongodb').MongoClient;

module.exports = class MongoWrapper {
  constructor(url, appDb, clientOptions = {useNewUrlParser: true, useUnifiedTopology: true}) {
    this.url = url;
    this.appDb = appDb;
    this.mongo;
    this.clientOptions = clientOptions;
  }

  async init() {
    try {
      this.mongo = await MongoClient.connect(this.url, this.clientOptions);
    } catch(error) {
      throw(error);
    }
  }

  async destroy() {
    this.mongo.close();
  }

  async deleteOne(collection, filter={}) {
    try {
      validateCollection(collection);
      const dbo = await this.mongo.db(this.appDb);

      return await dbo.collection(collection)
        .deleteOne(filter);
    } catch (e) {
      throw e;
    }
  }

  async deleteMany(collection, filter={}) {
    try {
      validateCollection(collection);
      const dbo = await this.mongo.db(this.appDb);

      return await dbo.collection(collection)
        .deleteMany(filter);
    } catch (e) {
      throw e;
    }
  }

  async find(collection, filter={}, projection={}) {
    try {
      validateCollection(collection);
      const dbo = await this.mongo.db(this.appDb);

      return await dbo.collection(collection)
        .find(filter)
        .project(projection)
        .toArray();
    } catch (e) {
      throw e;
    }
  }

  async updateOne(collection, filter, update, upsert=false) {
    try {
      validateCollection(collection);
      validateFilter(filter);
      validateUpdate(update);
      const dbo = await this.mongo.db(this.appDb);

      return await dbo.collection(collection)
        .updateOne(filter, {
          $set: update
        }, {
          upsert: upsert
        });
    } catch (e) {
      throw e;
    }
  }

  async updateMany(collection, filter, update) {
    try {
      validateCollection(collection);
      validateFilter(filter);
      validateUpdate(update);
      const dbo = await this.mongo.db(this.appDb);

      return await dbo.collection(collection)
        .updateMany(filter, {
          $set: update
        });
    } catch (e) {
      throw e;
    }
  }
};

function validateCollection(collection) {
  if (!collection) {
    throw new Error(`Collection parameter is required`);
  }
}

function validateFilter(filter) {
  if (!filter) {
    throw new Error(`Filter parameter is required`);
  }
}

function validateUpdate(update) {
  if (!update) {
    throw new Error(`Update parameter is required`);
  }
}