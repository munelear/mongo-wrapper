const MongoWrapper = require('./mongoWrapper');
const appDb = "test-db";
const collection = "testCollection";
const MongoClient = require('mongodb').MongoClient;

let mongo;

// wipe out all DB entries
beforeEach(async () => {
  mongo = new MongoWrapper(process.env.MONGO_URL, appDb);
  await mongo.init();

  await mongo.deleteMany(collection, {});
});

afterEach(async () => {
  await mongo.destroy();
});

describe('init', () => {
  test('it should surface any mongo errors', async () => {
    const connectError = new Error(`Unable to connect`);
    const connectSpy = jest.spyOn(MongoClient, 'connect').mockImplementation(() => {return Promise.reject(connectError)});
    try {
      await mongo.init();
    } catch(error) {
      expect(error).toBe(connectError);
    }
    expect.assertions(1);
    connectSpy.mockRestore();
  });
});


describe('deleteOne', () => {
  test('it should require a collection parameter', async () => {
    try {
      await mongo.deleteOne();
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should surface any mongo errors', async () => {
    try {
      await mongo.deleteOne(collection, 'gibberish');
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should delete a matching doc in the collection', async () => {
    const mockUserKey = {_id: 'some-user-id'};
    let mockUser = {...mockUserKey, name: 'John'};

    await mongo.updateOne(collection, mockUserKey, mockUser, true);
    expect(await mongo.find(collection, mockUserKey)).toEqual([mockUser]);

    await mongo.deleteOne(collection, mockUserKey);
    expect(await mongo.find(collection, mockUserKey)).toEqual([]);
  });

  test('it should delete any matching doc in the collection when no filter provided', async () => {
    let mockUserKey1 = {_id: 'some-user-id'};
    let mockUser1 = {...mockUserKey1, name: 'John'};

    await mongo.updateOne(collection, mockUserKey1, mockUser1, true);

    let mockUserKey2 = {_id: 'some-user-id2'};
    let mockUser2 = {...mockUserKey2, name: 'John'};
    await mongo.updateOne(collection, mockUserKey2, mockUser2, true);

    expect(await mongo.find(collection)).toEqual([mockUser1, mockUser2]);

    await mongo.deleteOne(collection);
    expect((await mongo.find(collection)).length).toEqual(1);
  });
});

describe('deleteMany', () => {
  test('it should require a collection parameter', async () => {
    try {
      await mongo.deleteMany();
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should surface any mongo errors', async () => {
    try {
      await mongo.deleteMany(collection, 'gibberish');
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should delete all matching docs in the collection', async () => {
    let mockUserKey1 = {_id: 'some-user-id'};
    let mockUser1 = {...mockUserKey1, name: 'John'};

    await mongo.updateOne(collection, mockUserKey1, mockUser1, true);

    let mockUserKey2 = {_id: 'some-user-id2'};
    let mockUser2 = {...mockUserKey2, name: 'John'};
    await mongo.updateOne(collection, mockUserKey2, mockUser2, true);

    // delete all Johns
    await mongo.deleteMany(collection, {name: 'John'});

    expect(await mongo.find(collection, {name: 'John'})).toEqual([]);
  });

  test('it should delete all docs in the collection when no filter provided', async () => {
    let mockUserKey1 = {_id: 'some-user-id'};
    let mockUser1 = {...mockUserKey1, name: 'John'};

    await mongo.updateOne(collection, mockUserKey1, mockUser1, true);

    let mockUserKey2 = {_id: 'some-user-id2'};
    let mockUser2 = {...mockUserKey2, name: 'Jack'};
    await mongo.updateOne(collection, mockUserKey2, mockUser2, true);

    expect(await mongo.find(collection)).toEqual([mockUser1, mockUser2]);

    // delete all
    await mongo.deleteMany(collection);

    expect(await mongo.find(collection)).toEqual([]);
  });
});

describe('find', () => {
  test('it should require a collection parameter', async () => {
    try {
      await mongo.find();
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('find should surface any mongo errors', async () => {
    try {
      await mongo.find(collection, []);
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should find all matching docs in the collection', async () => {
    let mockUserKey1 = {_id: 'some-user-id'};
    let mockUser1 = {...mockUserKey1, name: 'John'};

    await mongo.updateOne(collection, mockUserKey1, mockUser1, true);

    let mockUserKey2 = {_id: 'some-user-id2'};
    let mockUser2 = {...mockUserKey2, name: 'John'};
    await mongo.updateOne(collection, mockUserKey2, mockUser2, true);

    // find all Johns
    expect(await mongo.find(collection, {name: 'John'})).toEqual([mockUser1, mockUser2]);
  });

  test('it should support projections', async () => {
    let mockUserKey = {_id: 'some-user-id'};
    let projectFields = {...mockUserKey, name: 'Jenny', phone: '867-5309'}
    let mockUser = {...mockUserKey, ...projectFields, location: 'The wall', status: 'happy'};

    await mongo.updateOne(collection, mockUserKey, mockUser, true);

    // confirm doc contains all fields
    expect(await mongo.find(collection, mockUserKey)).toEqual([mockUser]);
    // can use projections to limit returned fields
    expect(await mongo.find(collection, mockUserKey, {name:1, phone:1})).toEqual([projectFields]);
  });
});

describe('updateOne', () => {
  test('it should require a collection parameter', async () => {
    try {
      await mongo.updateOne();
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should error when no filter specified', async () => {
    try {
      await mongo.updateOne(collection);
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should error when no update specified', async () => {
    try {
      await mongo.updateOne(collection, {});
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should surface any mongo errors', async () => {
    try {
      await mongo.updateOne(collection, 'gibberish');
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should insert a doc into collection with upsert', async () => {
    const mockUserKey = {_id: 'some-user-id'};
    const mockUser = {...mockUserKey, name: 'John'};

    await mongo.updateOne(collection, mockUserKey, mockUser, true);

    expect(await mongo.find(collection, mockUserKey)).toEqual([mockUser]);
  });

  test('it should not insert a doc into collection with upsert=false', async () => {
    const mockUserKey = {_id: 'some-user-id'};
    const mockUser = {...mockUserKey, name: 'Jack'};

    await mongo.updateOne(collection, mockUserKey, mockUser);

    expect(await mongo.find(collection, mockUserKey)).toEqual([]);
  });

  test('it should update a matching doc in the collection', async () => {
    const mockUserKey = {_id: 'some-user-id'};
    let mockUser = {...mockUserKey, name: 'John'};

    // insert John
    await mongo.updateOne(collection, mockUserKey, mockUser, true);
    expect(await mongo.find(collection, mockUserKey)).toEqual([mockUser]);

    mockUser = {...mockUserKey, name: 'Jack'};
    // change John to Jack
    await mongo.updateOne(collection, mockUserKey, mockUser);
    expect(await mongo.find(collection, mockUserKey)).toEqual([mockUser]);
  });
});

describe('updateMany', () => {
  test('it should require a collection parameter', async () => {
    try {
      await mongo.updateMany();
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should error when no filter specified', async () => {
    try {
      await mongo.updateMany(collection);
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should error when no update specified', async () => {
    try {
      await mongo.updateMany(collection, {});
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should surface any mongo errors', async () => {
    try {
      await mongo.updateMany(collection, 'gibberish');
    } catch(error) {
      expect(error).toEqual(expect.any(Error));
    }
    expect.assertions(1);
  });

  test('it should update all matching docs in the collection', async () => {
    let mockUserKey1 = {_id: 'some-user-id'};
    let mockUser1 = {...mockUserKey1, name: 'John'};

    await mongo.updateOne(collection, mockUserKey1, mockUser1, true);

    let mockUserKey2 = {_id: 'some-user-id2'};
    let mockUser2 = {...mockUserKey2, name: 'John'};
    await mongo.updateOne(collection, mockUserKey2, mockUser2, true);

    const jack = {name: 'Jack'};

    // rename all Johns to Jack
    await mongo.updateMany(collection, {name: 'John'}, jack);

    expect(await mongo.find(collection, jack)).toEqual([{...mockUserKey1, ...jack}, {...mockUserKey2, ...jack}]);
  });
});